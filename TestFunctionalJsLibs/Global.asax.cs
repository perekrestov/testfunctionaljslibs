﻿using System;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;

namespace TestFunctionalJsLibs
{
	public class Global : HttpApplication
	{
		protected void Application_Start(object sender, EventArgs e)
		{
			// WebApi		
			HttpConfiguration configuration = GlobalConfiguration.Configuration;
			configuration.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
			configuration.MapHttpAttributeRoutes();
			configuration.EnsureInitialized();
		}

		protected void Session_Start(object sender, EventArgs e)
		{
		}

		protected void Application_BeginRequest(object sender, EventArgs e)
		{
		}

		protected void Application_AuthenticateRequest(object sender, EventArgs e)
		{
		}

		protected void Application_Error(object sender, EventArgs e)
		{
		}

		protected void Session_End(object sender, EventArgs e)
		{
		}

		protected void Application_End(object sender, EventArgs e)
		{
		}
	}
}