﻿using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Web.Http;
using Dapper;
using TestFunctionalJsLibs.Model;

namespace TestFunctionalJsLibs.api
{
	[RoutePrefix("api/users")]
	public class UsersController : ApiController
	{
		// GET api/<controller>
		[Route]
		public async Task<IEnumerable<User>> Get()
		{
			using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["appulate"].ConnectionString))
			{
				await connection.OpenAsync();
				IEnumerable<User> users = await connection.QueryAsync<User>("select * from [User]");
				return users;
			}
		}


		// GET api/<controller>/5
		[Route("{id:int}")]
		public string Get(int id)
		{
			return "value";
		}

		// POST api/<controller>
		public void Post([FromBody] string value)
		{
		}

		// PUT api/<controller>/5
		public void Put(int id, [FromBody] string value)
		{
		}

		// DELETE api/<controller>/5
		public void Delete(int id)
		{
		}
	}
}